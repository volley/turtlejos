package turtlejos;

import java.io.*;
import java.net.Socket;

class ClientSocket {

  private final Object lock = new Object();

  private static final String host = "10.0.1.1";
  private static final int port = 9998;

  private Socket socket;
  private Reader reader;
  private Writer writer;

  private Thread thread;
  private int idleSeconds;

  private void startThread() {
    synchronized (lock) {
      if (thread == null) {
        thread = new Thread(threadRunnable);
        thread.setDaemon(true);
        thread.setName("ClientSocketTimer");
        thread.start();
      }
    }
  }

  private void stopThread() {
    synchronized (lock) {
      thread = null;
    }
  }

  private final Runnable threadRunnable = new Runnable() {
    @Override
    public void run() {
      synchronized (lock) {
        while (thread == Thread.currentThread()) {
          try {
            lock.wait(1000);
          } catch (InterruptedException e) {
            break;
          }
          idleSeconds++;
          if (idleSeconds >= 5) {
            closeImpl();
          }
        }
      }
    }
  };

  private boolean openImpl() {
    synchronized (lock) {
      try {
        if (socket == null) {
          System.out.println("ClientSocket: Connecting to " + host + ":" + port + "...");
          socket = new Socket(host, port);
          reader = new InputStreamReader(socket.getInputStream());
          writer = new OutputStreamWriter(socket.getOutputStream());
          System.out.println("ClientSocket: Connected");
          startThread();
          idleSeconds = 0;
        }
        return true;
      } catch (IOException ie) {
        ie.printStackTrace();
        closeImpl();
        return false;
      }
    }
  }

  private void closeImpl() {
    stopThread();
    synchronized (lock) {
      try {
        if (socket != null) {
          socket.close();
          System.out.println("ClientSocket: Disconnected");
        }
      } catch (IOException ie) {
        ie.printStackTrace();
      } finally {
        socket = null;
        reader = null;
        writer = null;
      }
    }
  }

  boolean issue(String command) {
    boolean success = false;
    synchronized (lock) {
      if (openImpl()) {
        try {
          writer.write(command + "\n");
          writer.flush();
          while (true) {
            final int ch = reader.read();
            if (ch == '+') {
              success = true;
              break;
            } else if (ch == '-' || ch == -1) {
              break;
            }
          }
        } catch (IOException e) {
          e.printStackTrace();
          closeImpl();
        }
        idleSeconds = 0;
      }
    }
    return success;
  }

}
