package turtlejos;

public class Demo {

  public static void main(String[] args) throws InterruptedException {
    TurtleClient turtleClient = new TurtleClient();
    turtleClient.forward(100);
    turtleClient.left(90);
    Thread.sleep(9000);
    turtleClient.right(90);
    turtleClient.forward(-100);
  }

}
