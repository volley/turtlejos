package turtlejos;

public class TurtleClient {

  private final ClientSocket socket = new ClientSocket();

  public TurtleClient() {
  }

  private boolean issue(String command) {
    System.out.println("TurtleClient: " + command);
    final boolean result = socket.issue(command);
    System.out.println("TurtleClient: " + command + " => " + result);
    return result;
  }

  public void forward(int length) {
    issue("f" + length);
  }

  public void left(int angle) {
    issue("l" + angle);
  }

  public void right(int angle) {
    left(-angle);
  }

  public void penUp() {
    issue("u");
  }

  public void penDown() {
    issue("d");
  }

}
