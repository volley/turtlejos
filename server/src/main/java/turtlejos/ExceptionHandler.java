package turtlejos;

public interface ExceptionHandler {

    void onException(Exception e);

}
