package turtlejos;

import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;

class ExecutorTurtle implements ITurtle {

    // todo: a boat-load of code for a very simple function

    private final ITurtle turtle;
    private final Executor executor;

    ExecutorTurtle(ITurtle turtle, Executor executor) {
        this.turtle = turtle;
        this.executor = executor;
    }

    @Override
    public void forward(final int length) {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.forward(length);
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void left(final int angle) {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.left(angle);
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void penUp() {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.penUp();
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void penDown() {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.penDown();
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void penToggle() {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.penToggle();
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void trimPenUp() {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.trimPenUp();
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }

    @Override
    public void trimPenDown() {
        final Semaphore semaphore = new Semaphore(0);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                turtle.trimPenDown();
                semaphore.release();
            }
        });
        semaphore.acquireUninterruptibly();
    }
}
