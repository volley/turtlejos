package turtlejos;

public interface ITurtle {

    /**
     * @param length Negative = backward
     */
    void forward(int length);

    /**
     * @param angle Degrees. Negative = right.
     */
    void left(int angle);

    void penUp();

    void penDown();

    void penToggle();

    void trimPenUp();

    void trimPenDown();

}
