package turtlejos;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;

class IdleExecutor implements Executor {

    private final Runnable onIdle;
    private final long idleTimeNanos;
    private final ExceptionHandler exceptionHandler;

    private final Runnable shutdownRunnable = new Runnable() {
        @Override
        public void run() {
            // No-op
        }
    };

    private final Deque<Runnable> queue = new ArrayDeque<Runnable>();
    private Thread thread;
    private boolean isIdle = true;

    IdleExecutor(Runnable onIdle, long idleTimeNanos, ExceptionHandler exceptionHandler) {
        this.onIdle = onIdle;
        this.idleTimeNanos = idleTimeNanos;
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void execute(Runnable command) {
        synchronized (queue) {
            queue.add(command);
            queue.notify();
        }
    }

    void startDispatcherThread() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                loop();
            }
        });
        thread.setDaemon(false);
        thread.start();
        // todo: shutdown hook
    }

    private void loop() {
        //noinspection InfiniteLoopStatement
        while (true) {
            final Runnable runnable = poll();
            try {
                if (runnable == shutdownRunnable) {
                    break;
                } else if (runnable != null) {
                    isIdle = false;
                    runnable.run();
                } else if (!isIdle) {
                    isIdle = true;
                    onIdle.run();
                }
            } catch (Exception e) {
                exceptionHandler.onException(e);
            }
        }
    }

    void shutdown() throws InterruptedException {
        execute(shutdownRunnable);
        thread.join();
    }

    private Runnable poll() {
        final long idleTimeoutAt = System.nanoTime() + idleTimeNanos;
        synchronized (queue) {
            while (true) {
                final Runnable poll = queue.poll();
                if (poll != null) {
                    return poll;
                }
                if (System.nanoTime() >= idleTimeoutAt) {
                    return null;
                }
                try {
                    queue.wait(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
