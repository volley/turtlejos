package turtlejos;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import turtlejos.ev3.Turtle;
import turtlejos.ev3.led.LedColor;
import turtlejos.ev3.led.LedController;
import turtlejos.ev3.led.LedMode;
import turtlejos.net.ClientRequestHandler;
import turtlejos.net.ClientThread;
import turtlejos.net.Server;
import turtlejos.net.SocketHandler;

import java.io.IOException;
import java.net.Socket;

public class Main {

    private static long nanosFromMs(long ms) {
        return ms * 1000000;
    }

    private enum Mode {
        Main,
        Move,
        Calibrate
    }

    public static void main(String[] args) throws Exception {

        final LedController ledController = new LedController();
        ledController.startThread();
        ledController.setPattern(LedColor.Yellow, LedMode.SlowBlink);

        final Turtle turtle = new Turtle();

        final Runnable onServerIdle = new Runnable() {
            @Override
            public void run() {
                turtle.onIdle();
                ledController.setPattern(LedColor.Green, LedMode.Static, 500);
            }
        };

        final ExceptionHandler exceptionHandler = new ExceptionHandler() {
            @Override
            public void onException(Exception e) {
                ledController.setPattern(LedColor.Red, LedMode.RapidBlink, 2000);
            }
        };

        final IdleExecutor executor = new IdleExecutor(onServerIdle, nanosFromMs(5000), exceptionHandler);

        final ExecutorTurtle executorTurtle = new ExecutorTurtle(turtle, executor);

        executor.startDispatcherThread();

        final ClientRequestHandler clientRequestHandler = new ClientRequestHandler(executorTurtle);

        Server server = new Server(new SocketHandler() {
            @Override
            public void accept(Socket socket) throws IOException {
              socket.setKeepAlive(true);
                final ClientThread clientThread = new ClientThread(socket, clientRequestHandler);
                clientThread.start();
            }
        });

        server.open();

        ledController.setPattern(LedColor.Green, LedMode.Static, 1000);

        Mode mode = Mode.Main;

        boolean keepGoing = true;
        while (keepGoing) {

            LCD.clear();
            LCD.drawString("TurtleJOS - " + mode, 0, 0);
            switch (mode) {
                case Main:
                    LCD.drawString("ESC: Exit", 0, 2);
                    LCD.drawString("Up: Manual control", 0, 3);
                    LCD.drawString("Enter: Calibrate pen", 0, 4);
                    break;
                case Calibrate:
                    LCD.drawString("ESC: Back", 0, 2);
                    LCD.drawString("Up/down: Pen state", 0, 3);
                    LCD.drawString("Left/right: Adjust", 0, 4);
                    break;
                case Move:
                    LCD.drawString("ESC: Back", 0, 2);
                    LCD.drawString("Up/down: Fwd/back", 0, 3);
                    LCD.drawString("Left/right: Turn 90", 0, 4);
                    LCD.drawString("Enter: Toggle pen", 0, 5);
                    break;
            }
            LCD.refresh();

            final int button = Button.waitForAnyPress();

            switch (mode) {
                case Main:
                    switch (button) {
                        case Button.ID_ESCAPE:
                            keepGoing = false;
                            break;
                        case Button.ID_UP:
                            mode = Mode.Move;
                            break;
                        case Button.ID_ENTER:
                            mode = Mode.Calibrate;
                            break;
                    }
                    break;
                case Move:
                    switch (button) {
                        case Button.ID_ESCAPE:
                            mode = Mode.Main;
                            break;
                        case Button.ID_UP:
                            executorTurtle.forward(50);
                            break;
                        case Button.ID_DOWN:
                            executorTurtle.forward(-50);
                            break;
                        case Button.ID_LEFT:
                            executorTurtle.left(90);
                            break;
                        case Button.ID_RIGHT:
                            executorTurtle.left(-90);
                            break;
                        case Button.ID_ENTER:
                            executorTurtle.penToggle();
                            break;
                        default:
                            break;
                    }
                    break;
                case Calibrate:
                    switch (button) {
                        case Button.ID_ESCAPE:
                            mode = Mode.Main;
                            break;
                        case Button.ID_UP:
                            executorTurtle.penUp();
                            break;
                        case Button.ID_DOWN:
                            executorTurtle.penDown();
                            break;
                        case Button.ID_LEFT:
                            executorTurtle.trimPenUp();
                            break;
                        case Button.ID_RIGHT:
                            executorTurtle.trimPenDown();
                            break;
                        default:
                            break;
                    }
                    break;
            }

        }

        ledController.setPattern(LedColor.Red, LedMode.SlowBlink);

        executor.shutdown();

        turtle.onIdle();

        server.close();

        ledController.stopThread();
        ledController.clearPattern();
    }

}
