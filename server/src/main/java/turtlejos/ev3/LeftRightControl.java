package turtlejos.ev3;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

class LeftRightControl {

  private static final int acceleration = 100;

    private RegulatedMotor motorLeft;
    private RegulatedMotor motorRight;

  private int forwardSpeed = 300;
  private int turnSpeed = 220;

    private float forwardFactor = 1f;

    public int getForwardSpeed() {
        return forwardSpeed;
    }

    public void setForwardSpeed(int forwardSpeed) {
        this.forwardSpeed = forwardSpeed;
    }

    public int getTurnSpeed() {
        return turnSpeed;
    }

    public void setTurnSpeed(int turnSpeed) {
        this.turnSpeed = turnSpeed;
    }

    public float getForwardFactor() {
        return forwardFactor;
    }

    public void setForwardFactor(float forwardFactor) {
        this.forwardFactor = forwardFactor;
    }

    private void startSynchronization() {
        motorLeft.startSynchronization();
    }

    private void endSynchronizationAndWaitComplete() {
        motorLeft.endSynchronization();

        motorLeft.waitComplete();
        motorRight.waitComplete();
    }

    private void rotateInternal(int left, int right, int speed) {
        if (left != 0 || right != 0) {
            ensureOpen();
            setSpeed(speed);
            startSynchronization();
            {
                motorLeft.rotate(left, true);
                motorRight.rotate(right, true);
            }
            endSynchronizationAndWaitComplete();
        }
    }

    void forward(int length) {
        final int angle = (int) (length * forwardFactor);
        rotateInternal(angle, angle, forwardSpeed);
    }

    void left(int turtleAngle) {
        if (turtleAngle != 0) {
          final int motorAngle = turtleAngle; // apply factor in client code
            rotateInternal(-motorAngle, motorAngle, turnSpeed);
        }
    }

    void onIdle() {
        if (motorLeft != null) {
//            System.out.println("LeftRightControl.onIdle");
        }
        closeIfOpen();
    }

    private void setSpeed(int speed) {
        motorLeft.setSpeed(speed);
        motorRight.setSpeed(speed);
    }

    private void ensureOpen() {
        if (motorRight == null) {
            motorRight = new EV3LargeRegulatedMotor(MotorPort.B);
          motorRight.setAcceleration(acceleration);
        }
        if (motorLeft == null) {
            motorLeft = new EV3LargeRegulatedMotor(MotorPort.A);
          motorLeft.setAcceleration(acceleration);
            motorLeft.synchronizeWith(new RegulatedMotor[]{motorRight});
        }
    }

    private void closeIfOpen() {
        if (motorLeft != null) {
            motorLeft.close();
            motorLeft = null;
        }
        if (motorRight != null) {
            motorRight.close();
            motorRight = null;
        }
    }

}
