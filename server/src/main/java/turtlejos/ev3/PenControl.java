package turtlejos.ev3;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

class PenControl {

  private static final int trimStep = 100;

    private RegulatedMotor motor;

    private boolean isUp = false;

  private int range = 1000;

    void penUp() {
        if (!isUp) {
            isUp = true;
            ensureOpen();
            motor.rotate(range);
        }
    }

    void penDown() {
        if (isUp) {
            isUp = false;
            ensureOpen();
            motor.rotate(-range);
        }
    }

    void onIdle() {
        if (motor != null) {
//            System.out.println("PenControl.onIdle");
        }
        closeIfOpen();
    }

    private void ensureOpen() {
        if (motor == null) {
            motor = new EV3LargeRegulatedMotor(MotorPort.C);
            motor.setAcceleration(1200);
            motor.setSpeed(600);
        }
    }

    private void closeIfOpen() {
        if (motor != null) {
            motor.close();
            motor = null;
        }
    }

    void penToggle() {
        if (isUp) {
            penDown();
        } else {
            penUp();
        }
    }

    private void trim(int angle) {
        ensureOpen();
        motor.rotate(angle);
        if (isUp) {
            // When up-position is adjusted away from the down-position, total range increases.
            range += angle;
        } else {
            // When down-position is adjusted towards the up-position, total range decreases.
            range -= angle;
        }
    }

    void trimPenUp() {
        trim(trimStep);
    }

    void trimPenDown() {
        trim(-trimStep);
    }
}
