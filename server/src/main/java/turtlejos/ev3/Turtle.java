package turtlejos.ev3;

import turtlejos.ITurtle;

public class Turtle implements ITurtle {

    private final LeftRightControl leftRightControl = new LeftRightControl();
    private final PenControl penControl = new PenControl();

    @Override
    public void forward(int length) {
        leftRightControl.forward(length);
    }

    @Override
    public void left(int angle) {
        leftRightControl.left(angle);
    }

    @Override
    public void penUp() {
        penControl.penUp();
    }

    @Override
    public void penDown() {
        penControl.penDown();
    }

    @Override
    public void penToggle() {
        penControl.penToggle();
    }

    @Override
    public void trimPenUp() {
        penControl.trimPenUp();
    }

    @Override
    public void trimPenDown() {
        penControl.trimPenDown();
    }

    public void onIdle() {
        leftRightControl.onIdle();
        penControl.onIdle();
    }

}
