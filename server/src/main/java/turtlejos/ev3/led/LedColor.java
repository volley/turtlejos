package turtlejos.ev3.led;

public enum LedColor {
    Green(1),
    Yellow(2),
    Red(3);

    private final int value;

    LedColor(int value) {
        this.value = value;
    }

    int getValue() {
        return value;
    }
}
