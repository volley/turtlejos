package turtlejos.ev3.led;

import lejos.hardware.Button;

public class LedController {

    private final Object lock = new Object();

    private long resetAt;

    private Thread thread;

    private final Runnable resetRunnable = new Runnable() {
        @Override
        public void run() {
            while (true) {
                synchronized (lock) {
                    if (resetAt != 0 && System.nanoTime() >= resetAt) {
                        Button.LEDPattern(0);
                        resetAt = 0;
                    }
                    final int timeOut = resetAt != 0 ? 100 : 10000; // todo: calculate delta (or use proper class for this :))
                    try {
                        lock.wait(timeOut);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }
    };

    public LedController() {
    }

    public void startThread() {
        thread = new Thread(resetRunnable);
        thread.setName("LedReset");
        thread.setDaemon(true);
        thread.start();
    }

    public void stopThread() {
        thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread = null;
    }

    public void clearPattern() {
        setPatternInternal(0, 0);
    }

    public void setPattern(LedColor ledColor, LedMode ledMode) {
        final int pattern = ledColor.getValue() + ledMode.getValue();
        setPatternInternal(pattern, 0);
    }

    public void setPattern(LedColor ledColor, LedMode ledMode, int ms) {
        final int pattern = ledColor.getValue() + ledMode.getValue();
        setPatternInternal(pattern, System.nanoTime() + (ms * 1000000L));
    }

    private void setPatternInternal(int pattern, long resetAt) {
        synchronized (lock) {
            Button.LEDPattern(pattern);
            this.resetAt = resetAt;
            this.lock.notify();
        }
    }

}
