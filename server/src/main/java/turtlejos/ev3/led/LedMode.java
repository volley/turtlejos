package turtlejos.ev3.led;

public enum LedMode {
    Static(0),
    SlowBlink(3),
    RapidBlink(6);

    private final int value;

    LedMode(int value) {
        this.value = value;
    }

    int getValue() {
        return value;
    }
}
