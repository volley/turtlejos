package turtlejos.net;

import turtlejos.ITurtle;

public class ClientRequestHandler {

    private final ITurtle turtle;

    public ClientRequestHandler(ITurtle turtle) {
        this.turtle = turtle;
    }

    void handleRequest(String line) {
        if (line.startsWith("f")) {
            final int length = Integer.parseInt(line.substring(1));
            turtle.forward(length);
        } else if (line.startsWith("b")) {
            final int length = Integer.parseInt(line.substring(1));
            turtle.forward(-length);
        } else if (line.startsWith("l")) {
            final int angle = Integer.parseInt(line.substring(1));
            turtle.left(angle);
        } else if (line.startsWith("r")) {
            final int angle = Integer.parseInt(line.substring(1));
            turtle.left(-angle);
        } else if (line.startsWith("u")) {
            turtle.penUp();
        } else if (line.startsWith("d")) {
            turtle.penDown();
        } else {
            throw new RuntimeException("? " + line);
        }
    }

}
