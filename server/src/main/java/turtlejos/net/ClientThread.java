package turtlejos.net;

import java.io.*;
import java.net.Socket;

public class ClientThread {

    private Socket socket;
    private ClientRequestHandler handler;

    public ClientThread(Socket socket, ClientRequestHandler handler) {
        this.socket = socket;
        this.handler = handler;
    }

    public void start() throws IOException {
        try {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                while (line.length() > 0 && !Character.isAlphabetic(line.charAt(0))) {
                    line = line.substring(1);
                }
                line = line.trim();
                if (line.equals("#")) {
                    break;
                }
                try {
                    handler.handleRequest(line);
                    writer.write("+");
                    writer.newLine();
                } catch (Exception e) {
                    writer.write("-");
                    writer.newLine();
                    e.printStackTrace();
                }
                writer.flush();
            }

        } finally {
            socket.close();
        }
    }
}
