package turtlejos.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket serverSocket;

    private final SocketHandler socketHandler;
    private Thread thread;

    public Server(SocketHandler socketHandler) {
        this.socketHandler = socketHandler;
    }

    public void open() throws IOException {
        serverSocket = new ServerSocket(9998);

        final Runnable target = new Runnable() {
            @Override
            public void run() {
                acceptLoop();
            }
        };

        thread = new Thread(target);
        thread.setName("AcceptThread");
        thread.setDaemon(true);
        thread.start();
    }

    public void close() throws Exception {
        serverSocket.close();
        thread.join();
    }

    private void acceptLoop() {
        while (!serverSocket.isClosed()) {
            try {
                final Socket socket = serverSocket.accept();
                socketHandler.accept(socket);
            } catch (IOException e) {
                if (!serverSocket.isClosed()) {
                    e.printStackTrace();
                }
            }
        }
    }

}
