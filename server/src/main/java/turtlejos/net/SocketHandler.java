package turtlejos.net;

import java.io.IOException;
import java.net.Socket;

public interface SocketHandler {

    void accept(Socket socket) throws IOException;

}
